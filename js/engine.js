$(function() {


  // Выпадающее меню


  // BEGIN of script for header submenu
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });



// BEGIN of script for banner-slider
$('.banner-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: true,

        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            adaptiveHeight: true
          }
        }
      ]
});

// BEGIN of script for about-us__slider
var aboutUsSlider  =  $('.about-us__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        adaptiveHeight: true
});
 $('.about-us__prev').click(function(){
 	$(aboutUsSlider).slick("slickPrev");
});
$('.about-us__next').click(function(){
 	$(aboutUsSlider).slick("slickNext");
 });


// BEGIN of script for monodoses-slider
var monodosesSlider  =  $('#monodoses-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.monodoses-slider__prev').click(function(){
 	$(monodosesSlider).slick("slickPrev");
});
$('.monodoses-slider__next').click(function(){
 	$(monodosesSlider).slick("slickNext");
 });


// BEGIN of script for #suppositories-slider
var suppositoriesSlider  =  $('#suppositories-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.suppositories-slider__prev').click(function(){
 	$(suppositoriesSlider).slick("slickPrev");
});
$('.suppositories-slider__next').click(function(){
 	$(suppositoriesSlider).slick("slickNext");
 });


// BEGIN of script for #stick-slider
var stickSlider  =  $('#stick-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.stick-slider__prev').click(function(){
 	$(stickSlider).slick("slickPrev");
});
$('.stick-slider__next').click(function(){
 	$(stickSlider).slick("slickNext");
 });



// BEGIN of script for #sachet-slider
var sachetSlider  =  $('#sachet-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.sachet-slider__prev').click(function(){
 	$(sachetSlider).slick("slickPrev");
});
$('.sachet-slider__next').click(function(){
 	$(sachetSlider).slick("slickNext");
 });



// BEGIN of script for #piece-slider
var pieceSlider  =  $('#piece-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.piece-slider__prev').click(function(){
 	$(pieceSlider).slick("slickPrev");
});
$('.piece-slider__next').click(function(){
 	$(pieceSlider).slick("slickNext");
 });



// BEGIN of script for #liquids-slider
var liquidsSlider  =  $('#liquids-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.liquids-slider__prev').click(function(){
 	$(liquidsSlider).slick("slickPrev");
});
$('.liquids-slider__next').click(function(){
 	$(liquidsSlider).slick("slickNext");
 });



// Слайдер карточки товара
$('.product-card__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.product-card__slider--nav',
    responsive: [{
        breakpoint: 767,
        settings: {
            arrows: true
        },
    }]

});
$('.product-card__slider--nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-card__slider',
    dots: false,
    arrows: false,
    focusOnSelect: true,
    

});



// BEGIN of script for #liquids-slider
var manufacturingPocessSlider  =  $('.manufacturing-process__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: true
});
 $('.liquids-slider__prev').click(function(){
 	$(manufacturingPocessSlider).slick("slickPrev");
});
$('.liquids-slider__next').click(function(){
 	$(manufacturingPocessSlider).slick("slickNext");
 });



// BEGIN of script for #liquids-slider
var packingProductsSlider  =  $('.packing-products__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});
 $('.packing-products__prev').click(function(){
 	$(packingProductsSlider).slick("slickPrev");
});
$('.packing-products__next').click(function(){
 	$(packingProductsSlider).slick("slickNext");
 });



var articleBoxHeight = $('.article-box__text').height();
$('.about-us__slider').before().css('top', '20px');
console.log(articleBoxHeight);


 // Табы
    $('.tabs-nav a').click(function() {
        var _id = $(this).attr('href');
        var _targetElement = $(this).parents('.tabs-wrapper').find('#' + _id);
        $('.tabs-nav a').removeClass('active');
        $(this).addClass('active');
        $('.tabs-item').removeClass('active');
        _targetElement.addClass('active');
        return false;

    });


    $('.tabs-mobile__btn').click(function() {
        var _targetElementParent = $(this).parents('.tabs-item');
        _targetElementParent.toggleClass('active-m');

        _targetElementParent.find('.tabs-item__body').slideToggle(600);
        $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
        return false;
    });


// BEGIN of script for .brands-slider
var brandsSlider  =  $('.brands-slider__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        // adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        fade: true,
        pauseOnHover: false,
        speed: 1000,
        dots: false
});



 $('.brands-slider__prev').click(function(){
 	$(brandsSlider).slick("slickPrev");
});
$('.brands-slider__next').click(function(){
 	$(brandsSlider).slick("slickNext");
 });





// Сайдбар активный пункт меню

$('.sidebar-item__toggle').click(function() {
    $(this).toggleClass('active');
    $(this).parent().toggleClass('active');
    $(this).next('.sidebar-item__body').fadeToggle();
})



// Поиск на сайте
$('.search > a').click(function() {
    $(this).next('.search-form').fadeToggle();
})


// Выпадающее меню каталога, работает до 768px
if($(window).width() > 767){
$('.dropdown-menu > li').hover(function() {
    $(this).children('ul').toggleClass('active');
})

}



// Включаем слайдеры на разрешении меньше 992px
if($(window).width() < 992){
// BEGIN of script for #liquids-slider
$('.equipment-five__wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true

});

$('.equipment-three__wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true

});

$('.stick .equipment-three__wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true,
        dots: true

});

$('.monodoses .equipment-three__wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true,
        dots: true

});

$('.suppositories .equipment-three__wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true,
        dots: true

});


$('#group-packing__slider1 .row').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true,

});


$('#group-packing__slider2 .row').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        // autoplay: true,
        // autoplaySpeed: 3000,
        pauseOnHover: false,
        speed: 1000,
        arrows: true,

});

}






// Выпвдвющее меню на разрешении < 768px

if($(window).width() < 768) {

    $('.dropdown > a').click(function() {
        $(this).parent().addClass('active');
    })

    $('.dropdown-level2 > a').click(function() {
        $(this).parent().addClass('active');
        $(this).parent('li').parent('ul').children('li').toggleClass('none');
        
    });




    $('.back-menu__lev1').click(function() {
        $(this).parent('li').parent('.dropdown-menu').parent('.dropdown').toggleClass('active');
    });

     $('.back-menu__lev2').click(function() {
        $(this).parent('li').parent('.dropdown-menu__level2').parent('.dropdown-level2').toggleClass('active');
        $('.dropdown-menu').children('.visible-xs, .dropdown-level2').removeClass('none');
    })


}

// Фильтры сайдбара на мобильных

$('.sidebar-toggle').click(function() {
    $(this).toggleClass('active');
    $('.sidebar-wrapper__filters').fadeToggle();
})












})